const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');

const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Computers, Cars, Other] = await Category.create({
        title: 'Computers',
        description: 'Computers, notebooks, brand technology for entertainment and household in the Computeruniverse online shop buy cheaply.'
    }, {
        title: 'Cars',
        description: 'An automobile repair shop'
    }, {
        title: 'Other',
        description: 'Other products'
    });

    await Product.create({
        title: 'Acer CB CB314-1HT-C9VY N4120, 4GB RAM, 64GB eMMC, Intel UMA, ChromeOS',
        price: 50000,
        description: 'Computer description',
        category: Computers,
        image: 'fixtures/comp1.jpg',
        user: 'CyMPJUoN4oLtr79-zEo-r'
    }, {
        title: 'Grand Cherokee SRT8',
        price: 350000,
        description: 'European plug-in hybrid vehicles like the 2021 BMW 530e tend to have a pretty specific purpose in mind: They’re meant to allow drivers to enter European cities that have congestion charges without being charged. Some cities like London or Antwerp charge drivers to enter the city centers in a car, but that charge is lessened or waived if your car is a zero-emissions vehicle. ',
        category: Cars,
        image: 'fixtures/car.jpeg',
        user: 'CyMPJUoN4oLtr79-zEo-r'
    }, {
        title: 'Hublot 2020 unworn Classic Fusion Classico Ultra-Thin 45mm',
        price: 80000,
        description: 'Classic Fusion Automatic Skeleton Dial Men\'s Watch 525NX0170LR',
        category: Other,
        image: 'fixtures/watch.webp',
        user: 'CyMPJUoN4oLtr79-zEo-r'
    });

    await User.create({
        username: 'user',
        password: '123',
        displayName: 'John Doe',
        phone: '+505555555',
        token: nanoid()
    }, {
        username: 'admin',
        password: '123',
        displayName: 'Mark Burton',
        phone: '+503333333',
        token: nanoid()
    });

    await mongoose.connection.close();
};

run().catch(console.error);