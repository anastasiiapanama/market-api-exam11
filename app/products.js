const express = require('express');
const path = require('path');
const multer = require('multer');

const Product = require("../models/Product");

const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    const category = {};

    if(req.query.category) {
        category.category = req.query.category;
    }

    const user = {};

    if(req.query.user) {
        category.user = req.query.user;
    }

    try {
        const products = await Product.find(category, user)
            .populate('category', '_id title')
            .populate('user');

        res.send(products);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findOne({_id: req.params.id})
            .populate('category', 'title')
            .populate('user');

        if (product) {
            res.send(product)
        } else (
            res.sendStatus(404)
        )

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const productRequestData = req.body;
        const user = req.user._id;
        console.log(user)
        productRequestData.user = req.user._id;

        if (req.file) {
            productRequestData.image = 'uploads/' + req.file.filename;
        }

        const product = new Product(productRequestData);
        await product.save();

        res.send(product);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const reqProduct = req.body;
        reqProduct.user = req.user._id;

        if (reqProduct.user) {
            const product = await Product.findOne({_id: req.params.id}).remove();

            res.send(product);
        } else {
            res.sendStatus(403);
        }

    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;